(if (getenv "AUTOPKGTEST_TMP")
    (let ((workspace (getenv "AUTOPKGTEST_TMP")))
      (shell-command "mv tests $AUTOPKGTEST_TMP")
      (message "Using %s/tests as writeable workspace" workspace)
      (shell-command "ln -s $AUTOPKGTEST_TMP/tests tests")
      (message "ln -s %s/tests tests" workspace)
      (shell-command "touch tests/project/1/.git")
      (message "touch tests/project/1/.git"))
  (let ((php-el-files '("lisp/php.el" "lisp/php-align.el" "lisp/php-face.el"
                        "lisp/php-project.el" "lisp/php-mode-debug.el"
                        "lisp/php-mode.el")))
    (while php-el-files
      (load-file (car php-el-files))
      (setq php-el-files (cdr php-el-files)))
    (package-generate-autoloads "php-mode" "lisp"))
  (shell-command "mv lisp/php-mode-autoloads.el debian/tmp/")
  (load-file "debian/tmp/php-mode-autoloads.el"))
